Picture selectori lisamine oma rakendusele

1. samm hangi omale Picture-selectori css ja js
	https://rvera.github.io/image-picker/
	seal allalaaditud zip-failist v�i projektist leiad kolm vajalikku faili
	image-picker-master.zip\image-picker-master\image-picker
		image-picker.css		stiilifail
		image-picker.js			vajalik javascripti teek
		image-picker.min.css	minimeeritud stiilifail
2. samm - kopeerid need failid oma projekti
		css-id n�iteks kausta Content
		js kausta Scripts
		(mina panin k�ik kausta COntent)

3. samm lisad viited neile failidele 
		mina panin otse view lehele, v�ib panna ka _layout lehele
		v�ib ka lisada bundlina (ei hakka pikealt selgitama)
		
		@section Scripts {
	    @Scripts.Render("~/bundles/jqueryval")
		@* nende ridade j�rele paigutasin j�rgneva *@
	    <script src="~/Scripts/jquery-3.3.1.js"></script>
	    <link href="~/Content/image-picker.css" rel="stylesheet" type="text/css">
		<script src="~/Content/image-picker.js"></script>

		@* hiljem lisame siia veel �he ploki *@

4. Hoolitsen, et on olemas meetod, mis v�ljastab faile
		minul on see People controlleris ja kannab nime DataFileContent
		ja talle antakse ette Pildifaili id

5. samm t�iendame natuke controllerit Edit meetodis
		enne View v�ljakutsumist lisan ViewBag-i listi pildiId-dega

		//ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "FileName", person.PictureId);
        ViewBag.PictureId = db.DataFiles.Select(x => x.Id).ToList();

		(hiljem, et saaks pickeril ka piltifailide nimesid n�idata)
		ViewBag.PictureList = db.DataFiles.ToDictionary(x => x.Id, x => x.FileName);

6. samm - muudan pisut selda select-listi, mille helper ehitab

		kommenteerin v�lja j�rgmise rea: 
		
		@*@Html.DropDownList("PictureId", null, htmlAttributes: new { @class = "form-control" })*@

		selle asemele ehitame select listi ise

		j�rgmine rida v�iks olla m�nes CSS-failis, aga ma panin ta siiasamma
		see, on piltide �hesuuruseks tegemiseks hea

		<style>img.small-picture {height: 80px}</style>
        
		j�rgmine plokk ehitab select listi - see koosneb <select> tagist
		ja selle vahel loetelu <optionitest>

		<select class="form-control image_picker show-html show-label" id="PictureId" name="PictureId" >
			@foreach (int x in ViewBag.PictureId)
            {
				<option data-img-class="small-picture" value="@x" @(Model.PictureId == x ? "selected" : "") data-img-src='@Url.Action("DataFileContent", "People", new { id = x })'>@ViewBag.PictureList[x]</option>
            }
        </select>

7. viimane samm on vajalik, et picture-picker k�ima hakkaks
		sealsamas, kus me enne panime scriptid ja css-id, lisane veel �he scripti

    <script>
        $(document).ready(function () {
            $("select").imagepicker();
            $("select.show-label").imagepicker({
                hide_select: true, // false, kui tahad selectorit n�ha
                show_label: true,  // false, kui ei taha failinimesid piltidel
		        });
	        })
    </script>

	